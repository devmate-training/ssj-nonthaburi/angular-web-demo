import { Component } from '@angular/core';
import {
    FormControl,
    FormGroup,
    ReactiveFormsModule,
    Validators,
} from '@angular/forms';
import { DummyService } from '../services/dummy.service';
import { catchError, of } from 'rxjs';
import { Route, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
    selector: 'app-login',
    standalone: true,
    imports: [ReactiveFormsModule],
    templateUrl: './login.component.html',
    styles: ``,
})
export class LoginComponent {
    loginForm = new FormGroup({
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required]),
    });

    isError = false;
    loading = false;

    constructor(
        private dummyService: DummyService,
        private userService: UserService,
        private router: Router
    ) {}

    onSubmit() {
        if (this.loginForm.valid) {
            const username = this.loginForm.get('username')?.value;
            const password = this.loginForm.get('password')?.value;
            this.loading = true;
            this.userService
                .login(username, password)
                .pipe(
                    catchError((err) => {
                        this.isError = true;
                        this.loading = false;
                        return of(err);
                    })
                )
                .subscribe((data) => {
                    this.loading = false;
                    if (data.token) {
                        this.isError = false;
                        const token = data.token;
                        sessionStorage.setItem('token', token);
                        this.router.navigateByUrl('/users/main');
                    } else {
                        this.isError = true;
                    }
                });
        } else {
            this.isError = true;
        }
    }
}
