import { Component } from '@angular/core';
import { RegisterUserFormComponent } from '../../components/register-user-form/register-user-form.component';
import { DummyService } from '../../services/dummy.service';
import { catchError, of } from 'rxjs';
import { Router } from '@angular/router';

@Component({
    selector: 'app-register',
    standalone: true,
    templateUrl: './register.component.html',
    styles: ``,
    imports: [RegisterUserFormComponent],
})
export class RegisterComponent {
    isError = false;
    loading = false;

    constructor(private dummyService: DummyService, private router: Router) {}

    onSubmited($event: any) {
        // console.log($event);
        const user = {
            username: $event.username,
            password: $event.password,
            firstName: $event.firstName,
            lastName: $event.lastName,
            email: $event.email,
            gender: $event.gender,
        };

        this.loading = true;

        this.dummyService
            .create(user)
            .pipe(
                catchError((err) => {
                    this.isError = true;
                    this.loading = false;
                    console.log(err);
                    return of(err);
                })
            )
            .subscribe((data) => {
                this.loading = false;
                console.log(data);
                this.router.navigateByUrl('/users/main');
            });
    }
}
