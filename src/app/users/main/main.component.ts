import { UpperCasePipe } from '@angular/common';
import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GenderPipe } from '../../pipes/gender.pipe';
import { ThaiDatePipe } from '../../pipes/thai-date.pipe';
import { UserTableComponent } from '../../components/tables/user-table/user-table.component';

@Component({
    selector: 'app-main',
    standalone: true,
    templateUrl: './main.component.html',
    styles: ``,
    imports: [UserTableComponent],
})
export class MainComponent {}
