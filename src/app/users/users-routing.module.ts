import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { RegisterComponent } from './register/register.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
    {
        path: '', //   /users
        redirectTo: 'main',
        pathMatch: 'full',
    },
    {
        path: 'main', //   /users/main
        component: MainComponent,
        title: 'หน้าหลัก : จัดการผู้ใช้งาน',
    },
    {
        path: 'register', //   /users/register
        component: RegisterComponent,
        title: 'ลงทะเบียนผู้ใช้งาน',
    },
    {
        path: 'edit/:id', //   /users/edit/123
        component: EditComponent,
        title: 'แก้ไขสมาชิก',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UsersRoutingModule {}
