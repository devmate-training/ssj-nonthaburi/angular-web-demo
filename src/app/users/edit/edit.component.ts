import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-edit',
    standalone: true,
    imports: [],
    templateUrl: './edit.component.html',
    styles: ``,
})
export class EditComponent {
    id: any = '';
    constructor(private route: ActivatedRoute) {
        this.id = this.route.snapshot.paramMap.get('id');
        // this.name = this.route.snapshot.paramMap.get('name');

        // Query params  ?vv=jjjjj
        this.route.queryParams.subscribe((params) => {
            console.log(params);
            const _params = params as any;
            console.log(_params.name);
        });

        console.log(this.id);
    }
}
