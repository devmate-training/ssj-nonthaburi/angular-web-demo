import { Routes } from '@angular/router';
import { HelloComponent } from './hello/hello.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { authGuard } from './auth.guard';

export const routes: Routes = [
    {
        path: '', // http://localhost:4200/
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: 'login', // http://localhost:4200/login
        component: LoginComponent,
        title: 'เข้าสู่ระบบ',
    },
    {
        path: 'hello', // http://localhost:4200/hello
        component: HelloComponent,
        // redirectTo: 'hello/world',
        // pathMatch: 'full',
    },
    {
        path: 'users',
        canActivate: [authGuard],
        loadChildren: () =>
            import('./users/users.module').then((m) => m.UsersModule),
    },
    {
        path: '**',
        component: NotFoundComponent,
    },
];
