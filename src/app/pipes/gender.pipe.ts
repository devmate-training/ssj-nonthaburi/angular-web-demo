import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'gender',
    standalone: true,
})
export class GenderPipe implements PipeTransform {
    transform(value: unknown, ...args: unknown[]): unknown {
        if (!value) {
            return 'ไม่ระบุ';
        }

        if (value === 'male') {
            return 'ชาย';
        } else if (value === 'female') {
            return 'หญิง';
        } else {
            return 'ไม่ทราบ';
        }
    }
}
