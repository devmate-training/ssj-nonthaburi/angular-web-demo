import { Pipe, PipeTransform } from '@angular/core';
import { DateTime } from 'luxon';

@Pipe({
    name: 'thaiDate',
    standalone: true,
})
export class ThaiDatePipe implements PipeTransform {
    transform(value: unknown, ...args: unknown[]): unknown {
        if (!value) {
            return 'ไม่ระบุ';
        }
        // 2022-11-01
        const date = DateTime.fromFormat(value as string, 'yyyy-MM-dd');

        if (!date.isValid) {
            return 'วันที่ไม่ถูกต้อง';
        }

        return date.setLocale('th').toLocaleString(DateTime.DATE_FULL);
    }
}
