import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
    AbstractControl,
    FormControl,
    FormGroup,
    ReactiveFormsModule,
    Validators,
} from '@angular/forms';

@Component({
    selector: 'app-register-user-form',
    standalone: true,
    imports: [ReactiveFormsModule],
    templateUrl: './register-user-form.component.html',
    styles: ``,
})
export class RegisterUserFormComponent {
    @Input() loading!: boolean;
    @Output() loadingChange = new EventEmitter();

    @Output() onSubmited = new EventEmitter();

    // Create from group
    registerForm = new FormGroup({
        username: new FormControl('', [
            Validators.minLength(4),
            Validators.maxLength(20),
            Validators.required,
        ]),
        password: new FormControl('', [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(20),
        ]),
        firstName: new FormControl('', [
            Validators.required,
            Validators.pattern('^[a-z]+$'),
            Validators.minLength(2),
            Validators.maxLength(50),
        ]),
        lastName: new FormControl('', [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(50),
        ]),
        email: new FormControl('', [
            Validators.required,
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ]),
        gender: new FormControl('female'),
    });

    onSubmit() {
        console.log(this.registerForm.valid);
        if (this.registerForm.valid) {
            this.onSubmited.emit(this.registerForm.value);
        }
    }

    get form(): { [key: string]: AbstractControl } {
        return this.registerForm.controls;
    }
}
