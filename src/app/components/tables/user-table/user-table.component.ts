import { UpperCasePipe } from '@angular/common';
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { ThaiDatePipe } from '../../../pipes/thai-date.pipe';
import { GenderPipe } from '../../../pipes/gender.pipe';
import { ModalChangePasswordComponent } from '../../modals/modal-change-password/modal-change-password.component';
import { DummyService } from '../../../services/dummy.service';
import { catchError, of } from 'rxjs';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'app-user-table',
    standalone: true,
    imports: [
        UpperCasePipe,
        ThaiDatePipe,
        GenderPipe,
        ModalChangePasswordComponent,
    ],
    templateUrl: './user-table.component.html',
    styles: ``,
})
export class UserTableComponent {
    @ViewChild(ModalChangePasswordComponent) modalChangePassword:
        | ModalChangePasswordComponent
        | undefined;

    users: any[] = [];
    isError = false;
    loading = false;

    total = 0;
    currentPage = 1;
    allPages: any[] = [];
    limit = 2;
    skip = 0;

    constructor(
        private dummyService: DummyService,
        private userServivce: UserService
    ) {}

    ngOnInit() {
        this.getAllUser();
    }

    getAllUser() {
        this.loading = true;
        this.userServivce
            .read(this.limit, this.skip)
            .pipe(
                catchError((error) => {
                    // console.error(error);
                    this.isError = true;
                    this.loading = false;
                    return of(error);
                })
            )
            .subscribe((data: any) => {
                this.loading = false;
                this.users = data.users;

                this.total = data.total;
                const pages = Math.ceil(this.total / this.limit);
                this.allPages = Array.from({ length: pages }, (_, i) => i + 1);
                console.log(pages);
                console.log(this.allPages);
            });
    }

    changePage(page: number) {
        this.currentPage = page;
        this.skip = (this.currentPage - 1) * this.limit;
        this.getAllUser();
    }

    changePassword(user: any) {
        this.modalChangePassword?.openModal(user);
    }

    onChangePassword($event: any) {
        console.log($event);
    }
}
