import { Component, ElementRef, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-modal-change-password',
    standalone: true,
    imports: [FormsModule],
    templateUrl: './modal-change-password.component.html',
    styles: ``,
})
export class ModalChangePasswordComponent {
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    password = '';
    firstName = '';

    constructor(private elementRef: ElementRef<HTMLElement>) {}

    closeModal() {
        const modal: any = this.elementRef.nativeElement.querySelector(
            '#modal-change-password'
        );

        if (modal) {
            modal.close();
        }
    }

    openModal(user: any) {
        this.firstName = user.firstName;
        this.password = '';

        const modal: any = this.elementRef.nativeElement.querySelector(
            '#modal-change-password'
        );

        if (modal) {
            modal.showModal();
        }
    }

    changePassword() {
        this.onChange.emit(this.password);
        this.closeModal();
    }
}
