import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class DummyService {
    constructor(private http: HttpClient) {}

    private getHeader(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`,
        });
    }
    read(limit: number = 10, skip: number = 0) {
        const headers = this.getHeader();
        const url = `https://dummyjson.com/auth/users?limit=${limit}&skip=${skip}`;
        return this.http.get(url, { headers });
    }

    create(user: any) {
        const headers = this.getHeader();
        const url = `https://dummyjson.com/users/add`;
        return this.http.post(url, user, { headers });
    }

    login(username: any, password: any) {
        const url = `https://dummyjson.com/auth/login`;
        return this.http.post(url, { username, password });
    }
}
