import { Injectable, signal } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    jwtHelper: JwtHelperService = new JwtHelperService();
    isLogged = signal(false);

    constructor(private router: Router) {}

    isAuthen() {
        const token = sessionStorage.getItem('token');

        if (token) {
            const isExpired = this.jwtHelper.isTokenExpired(token);
            if (isExpired) {
                this.isLogged.set(false);
                return this.router.navigateByUrl('/login');
            }

            this.isLogged.set(true);
            return true;
        } else {
            this.isLogged.set(false);
            return this.router.navigateByUrl('/login');
        }
    }

    logout() {
        sessionStorage.clear();
        this.isLogged.set(false);
    }
}
