import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class UserService {
    constructor(private http: HttpClient) {}

    private getHeader(): HttpHeaders {
        return new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: `Bearer ${sessionStorage.getItem('token')}`,
        });
    }

    login(username: any, password: any) {
        const url = `http://localhost:3000/login`;
        return this.http.post(url, { username, password });
    }

    read(limit: number = 10, skip: number = 0) {
        const headers = this.getHeader();
        const url = `http://localhost:3000/users?limit=${limit}&skip=${skip}`;
        return this.http.get(url, { headers });
    }
}
