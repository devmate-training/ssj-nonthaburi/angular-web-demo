import { Component, effect } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, RouterModule, RouterOutlet } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
    selector: 'app-root',
    standalone: true,
    imports: [RouterOutlet, FormsModule, RouterModule],
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
})
export class AppComponent {
    username: string = 'admin';
    gender = '';
    isLogged = false;

    constructor(private authService: AuthService, private router: Router) {
        effect(() => {
            console.log(`isLogged: ${this.authService.isLogged()}`);
            this.isLogged = this.authService.isLogged();
        });
    }

    logout() {
        this.authService.logout();
        this.router.navigateByUrl('/login');
    }

    onChange($event: any) {
        // console.log($event);
        const value = $event.target.value;
        console.log(value);
    }

    onClick() {
        console.log(this.gender);
    }
}
